<!DOCTYPE html>

<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>下田美咲Official | 株式会社ミサキ式</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="株式会社ミサキ式">
  <meta name="description" content="下田美咲Official. ニュース, プロフィール, イベント情報, ライブ情報, ファンクラブ案内.">
  <meta name="keywords" content="下田美咲, シモダミサキ, しもだみさき, shimoda, misaki, ミサキ式, misakishiki, シモリアン">
  <meta property="og:title" content="Shimoda Misaki Official Site">
  <meta property="og:site_name" content="Shimoda Misaki Official Site">
  <meta property="og:description" content="下田美咲Official. ニュース, プロフィール, イベント情報, ライブ情報, ファンクラブ案内.">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
  <link rel="icon" href="images/ico/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" type="image/x-icon" href="images/ico/favicon.ico">
</head>
<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<div id="wrap">
  <div id=header>
    <a href="/">Misaki Shimoda Official Site</a>
  </div>
  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse self-nav" id="nav-menu-1">
        <ul class="nav navbar-nav">
          <li><a href="news.html"><div class="nav-detail-icon"><i class="fa fa-info-circle fa-fw"></i> News</div></a></li>
          <li><a href="profile.html"><div class="nav-detail-icon"><i class="fa fa-street-view fa-fw"></i> Profile</div></a></li>
          <li><a href="project.html"><div class="nav-detail-icon"><i class="fa fa-plane fa-fw"></i> Project</div></a></li>
          <li><a href="counseling.html"><div class="nav-detail-icon"><i class="fa fa-comments fa-fw"></i> Counseling</div></a></li>
          <li><a href="bar.html"><div class="nav-detail-icon"><i class="fa fa-glass fa-fw"></i> Bar</div></a></li>
          <li><a href="release.html"><div class="nav-detail-icon"><i class="fa fa-play fa-fw"></i> Release</div></a></li>
          <li><a href="contact.html"><div class="nav-detail-icon"><i class="fa fa-paper-plane fa-fw"></i> Contact</div></a></li>
          <li><a href="https://note.mu/shimodamisaki" target="_blank"><div class="nav-detail-icon"><i class="fa fa-pencil-square-o fa-fw"></i> Note</div></a></li>
          <li><a href="https://twitter.com/shimodamisaki" target="_blank"><div class="nav-detail-icon"><i class="fa fa-twitter fa-fw"></i> Twitter</div></a></li>
          <li><a href="https://instagram.com/shimodamisaki815" target="_blank"><div class="nav-detail-icon"><i class="fa fa-camera-retro fa-fw"></i> Instagram</div></a></li>
          <li><a href="http://ameblo.jp/shimodamisaki/" target="_blank"><div class="nav-detail-icon"><i class="fa fa-rss fa-fw"></i> Blog</div></a></li>
          <li><a href="https://www.youtube.com/channel/UCEAMSFm4Ibr2kh1FKzg3dhw" target="_blank"><div class="nav-detail-icon"><i class="fa fa-youtube fa-fw"></i> Videos</div></a></li>
          <li><a href="https://itunes.apple.com/jp/app/jiaonashi-ci-dian/id1040126290?mt=8" target="_blank"><div class="nav-detail-icon"><i class="fa fa-mobile fa-fw"></i> App</div></a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="box"><?php
	header("Content-type: text/html; charset=utf-8");
	// exit();
	// $status = array(
	// 	'type'=>'success',
	// 	'message'=>'Thank you for sending!'
	// );
		$status = "Thank you for sending!";

    $name = @trim(stripslashes($_POST['name']));
    $email = @trim(stripslashes($_POST['email']));
    $subject = @trim(stripslashes($_POST['subject']));
    $message = @trim(stripslashes($_POST['message']));

    $email_from = $email;
    $email_to = 'm.sparkle@icloud.com';//replace with your email

    $body = 'Name: ' . $name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Subject: ' . $subject . "\n\n" . 'Message: ' . $message;

    $success = @mail($email_to, $subject, $body, 'From: <'.$email_from.'>');

    echo json_encode($status);
		// header('location: contact.html');
    die;?>
		</div>
		<div id=hooter>
			Copyright  &copy;2016<br/><a href="/"> shimodamisaki.com All Rights Reserved. </a>
		</div>
	</div>
</body>
</html>
