$("#modal-news11").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});

(function($) {
	var config = {
		"opening": ".opening",
		"element": [".lastname", ".firstname", ".website"],
		"speed": 600,
		"delay": 300,
		"range": 10
	};

	var referrer = document.referrer;
	var host = location.hostname;
	if(!referrer.match(host)) {
		setTimeout(function() {
			for(var i = 0, n = config.element.length; i < n; i++) {
				var $el = $(config.element[i]);
				var _top = $el.css("top").slice(2,-1);
				$el.css("top", +_top-config.range);
				$(config.element[i]).delay(config.delay*i).animate({
					"top": "+="+config.range,
					"opacity": 1
				}, config.speed, "swing");
			}
		}, 1000);

		setTimeout(function() {
			$(config.opening).fadeOut("slow", function() {
				$(this).remove();
			});
		}, 3000);
	} else {
		$(config.opening).hide();
	}

})(jQuery);
